set number
set nocompatible
syntax on
set encoding=utf-8
filetype off

" Esc to go to normal mode in terminal
:tnoremap <Esc> <C-\><C-n>

" Better split navigation ctrl+j instead of ctrl+w+j
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" Better split resize: ctrl+arrows
nnoremap <C-Up> <C-W>+
nnoremap <C-Down> <C-W>-
nnoremap <C-Right> <C-W>>
nnoremap <C-Left> <C-W><
" Go to new split when opened
set splitbelow
set splitright

"Multiple cursors
let g:multi_cursor_next_key='<C-b>'

"Solarized
set background=dark
colorscheme solarized

" Switch between files without having to save
set hidden

" Remap the leader key
let mapleader=","

"Always show statusline
set laststatus=2
:nmap , :

" Space jumps to next buffer
nnoremap <space> :bnext!<cr>
nnoremap <Tab> :bprevious!<cr>

"Proper markdown support
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

" Nerdtree
map <C-n> :NERDTreeToggle<CR>

" delimitMate
let b:delimitMate_expand_cr = 2
let b:delimitMate_expand_space = 1
let b:delimitMate_balance_matchpairs = 1
let delimitMate_excluded_ft = "md"
au FileType md, md let b:delimitMate_autoclose = 0

" TagBar
nmap <F8> :TagbarToggle<CR>

" Proper syntax highlighting form markdown
autocmd BufNewFile,BufReadPost *.md set filetype=markdown

" Arline
" let g:airline_theme="wombat"
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" indentLine
let g:indentLine_enabled = 1
"let g:indentLine_char = '-'

" vim-auto-save conf
"  enable autosame on vim startup
let g:auto_save = 1
" disable in insert mode
let g:auto_save_in_insert_mode = 0

" Neomake checks syntax on every buff save
autocmd! BufWritePost * Neomake

"Autoformat
let g:formatdef_happy_chappelier = '"astyle -A8 --indent=spaces=4 --convert-tabs --suffix=none --add-brackets --unpad-paren --pad-oper --pad-header --align-pointer=type --max-code-length=80 --lineend=linux"'
autocmd FileType c let g:formatters_cs = ['happy_chappelier']
autocmd FileType c noremap <F9> :Autoformat <CR>

call plug#begin('~/.config/nvim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'Raimondi/delimitMate'
Plug 'majutsushi/tagbar'
Plug 'bling/vim-airline'
"Plug 'bronson/vim-trailing-whitespace'
Plug 'ConradIrwin/vim-bracketed-paste'
Plug 'tpope/vim-fugitive'
Plug 'Yggdroot/indentLine'
Plug 'ervandew/supertab'
Plug 'derekwyatt/vim-scala'
Plug 'junegunn/goyo.vim'
Plug 'tpope/vim-unimpaired'
Plug 'sombr/vim-scala-worksheet'
Plug '907th/vim-auto-save'
Plug 'benekastah/neomake'
Plug 'terryma/vim-multiple-cursors'
Plug 'mhinz/vim-startify'
Plug 'sophacles/vim-processing'
Plug 'kshenoy/vim-signature'
Plug 'Chiel92/vim-autoformat'
Plug 'leafgarland/typescript-vim'
call plug#end()
"Plug 'lervag/vimtex'

filetype plugin indent on
